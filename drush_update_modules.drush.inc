<?php

function drush_update_modules_drush_command() {
  return array(
    'generate-update-module' => array(
      'description' => 'Generates an empty update module, ready for customization.',
      'arguments' => array(
        'name' => 'The name to use for the generated module.',
      ),
      'options' => array(
        'update-type' => array(
          'description' => 'Whether this update runs "before" or "after" normal updates. Defaults to "after".',
          'example-value' => 'after',
        ),
        'parent-dir' => array(
          'description' => 'The parent directory in which to generate the module. Specify the full path, relative to docroot. (Defaults to sites/all/modules/updates)',
          'example-value' => 'sites/all/modules/updates',
        ),
        'title' => array(
          'description' => 'Human-readable title to use for the module. Defaults to same as module machine name.',
        ),
        'desc' => array(
          'description' => 'Description for the module .info file. Defaults to empty string.',
        ),
      ),
      'aliases' => array('gum'),
      'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_SITE,
      'drush dependencies' => array('pm'),
    ),
    'remove-update-modules' => array(
      'description' => 'Removes update modules.',
      'bootstrap' => DRUSH_BOOTSTRAP_DRUPAL_FULL,
      'aliases' => array('rum'),
      'drush dependencies' => array('pm'),
      'options' => array(
        'parent-dir' => array(
          'description' => 'The parent directory in which update modules are stored. Specify the full path, relative to docroot. (Defaults to sites/all/modules/updates)',
          'example-value' => 'sites/all/modules/updates',
        ),
      ),
    ),
  );
}

/**
 * Generate a template update module.
 *
 * @param $name
 *   The name of the module to generate.
 */
function drush_drush_update_modules_generate_update_module($name) {
  if ($name == '') {
    return drush_set_error('NONAME', 'No module name provided');
  }

  if (count(func_get_args()) > 1) {
    return drush_set_error('MORENAMES', 'Can only operate on one name at a time');
  }

  if (!preg_match('/^[a-zA-Z_\x7f-\xff][a-zA-Z0-9_\x7f-\xff]*$/', $name)) {
    return drush_set_error('BADNAME', 'Invalid characters for machine name');
  }

  $info = array(
    'desc' => drush_get_option('desc', ''),
    'title' => drush_get_option('title', $name),
    'type' => drush_get_option('update-type', 'after'),
    'parent-dir' => drush_get_option('parent-dir', _drush_update_modules_get_parent_dir()),
    'core' => drush_drupal_major_version() . '.x',
  );

  if (!file_exists($info['parent-dir'])) {
    if (!drush_confirm(dt('Updates directory !path does not exist. Do you want to create it?', array('!path' => $info['parent-dir'])))) {
      return drush_user_abort();
    }
    drush_mkdir($info['parent-dir'], TRUE);
  }

  $tdir = $info['parent-dir'] . '/' . $name;
  if (file_exists($tdir)) {
    drush_log(dt('Module directory already exists at !dir', array('!dir' => $tdir)));
    return drush_set_error('MODULEEXISTS', 'Module already exists');
  }

  drush_mkdir($tdir);
  file_put_contents($tdir . "/$name.info", <<<EOT
name = {$info['title']}
description = "{$info['description']}"
update type = {$info['type']}
core = {$info['core']}

EOT
);

  file_put_contents($tdir . "/$name.module", <<<'EOT'
<?php

/**
 * Intentionally left blank.
 */

EOT
);

  file_put_contents($tdir . "/$name.install", <<<EOT
<?php

/**
 * Implements hook_install().
 */
function {$name}_install() {

}

EOT
);
  drush_log(dt('Update module created with .info, .module, and .install file at !dir', array('!dir' => $tdir)));
}

/**
 * Returns the relative path to the update modules parent directory.
 *
 * @return string
 *   A path relative to the drupal root.
 */
function _drush_update_modules_get_parent_dir() {
  require_once DRUSH_BASE_PATH . '/commands/pm/download.pm.inc';
  $base = _pm_download_destination('module');
  // Strip contrib dir, use updates base instead. Or just append, if "contrib"
  // is not there.
  $relative_root = substr($base, strlen(DRUPAL_ROOT . '/'));
  return preg_replace('/\/contrib$/', '', $relative_root) . '/updates';
}

/**
 * Uninstalls any installed update module.
 */
function drush_drush_update_modules_remove_update_modules() {
  $parent_dir = drush_get_option('parent-dir', _drush_update_modules_get_parent_dir() . '/removed');
  if (!drush_is_absolute_path($parent_dir)) {
    $parent_dir = DRUPAL_ROOT . '/' . $parent_dir;
  }

  $modules = array();
  foreach (system_rebuild_module_data() as $module => $data) {
    if (drush_update_module_is_applied($module)) {
      $source_path = dirname(DRUPAL_ROOT . '/' . $data->filename);
      $target_path = $parent_dir . '/' . $module;
      mkdir($target_path, 0755, TRUE);
      // Move the module in the uninstalled directory and rename the module
      // .info file so it will no longer be detected.
      if (rename($source_path, $target_path) && rename($target_path . '/' . $module . '.info', $target_path . '/' . $module . '.info.removed')) {
        $modules[] = $module;
        // The module is now removed, if we restore it we likely want to install
        // it again, so clear its entry.
        variable_del('drush_update_modules_' . $module);
      }
    }
  }

  // Clear uninstalled module data.
  if ($modules) {
    drush_log('Removed the following update modules: ' . implode(', ', $modules), 'ok');
    drupal_flush_all_caches();
  }
}

/**
 * Implements drush_hook_pre_COMMAND().
 */
function drush_drush_update_modules_pre_updatedb() {
  drush_bootstrap_max();
  drush_update_modules('before');
}

/**
 * Implements drush_hook_post_COMMAND().
 */
function drush_drush_update_modules_post_updatedb() {
  drush_bootstrap_max();
  drush_update_modules('after');
  drush_update_modules_uninstall();
}

/**
 * Enable update modules.
 *
 * @param $type
 *   The type as specified in update type=before|after
 */
function drush_update_modules($type) {
  // Get the data built during the validate phase
  $modules = array();

  foreach (system_rebuild_module_data() as $module => $data) {
    if (drush_update_module_is_valid($module) && !drush_update_module_is_applied($module) && $data->info['update type'] == $type) {
      $modules[] = $module;
    }
  }

  if (empty($modules)) {
    return drush_log(dt('There were no extensions that could be enabled.'), 'ok');
  }
  else {
    drush_print(dt('The following update modules will be enabled: !modules', array('!modules' => implode(', ', $modules))));
    if(!drush_confirm(dt('Do you really want to continue?'))) {
      return drush_user_abort();
    }
  }

  // Enable modules and pass dependency validation in form submit.
  if (!empty($modules)) {
    drush_include_engine('drupal', 'environment');
    drush_module_enable($modules);
  }
}

/**
 * Uninstalls existing update modules.
 */
function drush_update_modules_uninstall() {
  // Collect all installed update modules.
  $modules = array();
  foreach (system_rebuild_module_data() as $module => $data) {
    if (module_exists($module) && drush_update_module_is_valid($module)) {
      $modules[] = $module;
    }
  }
  // Uninstall update modules.
  if ($modules) {
    drush_log(dt('Uninstalling the following update modules: @modules.', array('@modules' => implode(', ', $modules))), 'ok');
    module_disable($modules);
    drupal_uninstall_modules($modules);
  }
}

/**
 * Checks whether a module is a valid update module.
 *
 * @param string $module
 *   The module name.
 *
 * @return bool
 *   TRUE if the module is an update module, FALSE otherwise.
 */
function drush_update_module_is_valid($module) {
  $module_data = system_rebuild_module_data();
  return !empty($module_data[$module]->info['update type']);
}

/**
 * Checks whether an update module was applied.
 *
 * @param string $module
 *   The module name.
 *
 * @return bool
 *   TRUE if the update was applied, FALSE otherwise.
 */
function drush_update_module_is_applied($module) {
  return drush_update_module_is_valid($module) && variable_get('drush_update_modules_' . $module);
}
